import csv
import argparse
import uuid

#curl types
PARTNERS = 'Partners'
CITIES = 'Cities'

parser = argparse.ArgumentParser()
parser.add_argument('--file', type=str)
parser.add_argument('--type', type=str)
args = parser.parse_args()
fileName = args.file	
curlType = args.type

MsgID = str(uuid.uuid4())
beginCmd = "curl --location --request POST '0:8080/mlm/v1/dropdown-values' \\\n--header 'Content-Type: application/json' \\\n--header 'Postman-Token: 667cfbd2-c9ba-4689-b26b-16f4f21bb07b' \\\n--header 'X-ACCESS-TOKEN: test_token' \\\n--header 'X-Client-ID: 2' \\\n--header 'cache-control: no-cache' \\\n--data-raw '{\n    \"MsgID\": \"%s\",\n    \"Resources\": [\n\t{\n"%MsgID
end="\n}';"
cmd = []

def isInValidData(data, curlType):
	if curlType == PARTNERS:
		return True if data["Done"] == 'TRUE' or data["CountryCode"] == '' or data["DropDownType"] == '' or data["Label"] == '' or data["Description"] == '' else False
	else:
		return True if data["Done"] == 'TRUE' or data["CountryCode"] == '' or data["DropDownType"] == '' or data["Label"] == '' or data["ParentDropDownType"] == '' or data["ParentDropDownID"] == '' else False

def buildPartnerResourceObj(data):
	resource = ''
	if data["DropDownType"] == "GatewayMerchant":
		resource += '\t\t"ParentDropDownType": '+'"{}"'.format(data["ParentDropDownType"])+',\n'
		resource += '\t\t"ParentDropDownID": '+data["ParentDropDownID"]+',\n'

	resource += '\t\t"Description": '+'"{}"'.format(data["Description"])+'\n'
	return resource

def buildCitiesResourceObj(data):
	resource = ''
	resource += '\t\t"ParentDropDownType": '+'"{}"'.format(data["ParentDropDownType"])+',\n'
	resource += '\t\t"ParentDropDownID": '+data["ParentDropDownID"]+'\n'
	return resource

with open(fileName, 'r') as csv_file:
	file = csv.DictReader(csv_file)
	for i, data in enumerate(file):	
		if isInValidData(data, curlType):
			continue
		else:
			resource = '\t\t"CountryCode": '+data["CountryCode"]+',\n'
			resource += '\t\t"DropDownType": '+'"{}"'.format(data["DropDownType"])+',\n'
			resource += '\t\t"Label": '+'"{}"'.format(data["Label"])+',\n'

			if curlType == PARTNERS:
				resource += buildPartnerResourceObj(data)
			else:
				resource += buildCitiesResourceObj(data)

			resource += '\t}\n]'
			print(beginCmd+resource+end)
